import sympy as sp

from scipy.optimize import fsolve
from math import sin


class step():
    def __init__(self, x):
        self.value = x

    def evaluate(self):
        return self.value

class ramp():
    def __init__(self):
        pass
        
    def evaluate(self, x):
        return x

class sinusoidal():
    def __init__(self):
        pass
        
    def evaluate(self, x):
        return sin(x)




