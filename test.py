import unittest
import inspect

from objects import*
from inputs import*
import math

class TestSim(unittest.TestCase):

    def test_agent_data_structure(self):
        heater = agent("a", 5, 1)
        self.assertEqual(heater.name, "a")

    def test_print_connections(self):
        agent1 = agent("Agent 1",1,5)
        agent2 = agent("Agent 2",1,5)
        agent3 = agent("Agent 3",1,5)

        agent1.add_link(agent2, agent3)

        expected = "Agent 1 is linked to: Agent 2 Agent 3 "
        
        self.assertEqual(expected, agent1.get_connections_string())

class TestStep(unittest.TestCase):
    def test_evaluate(self):
        step_obj = step(42)
        self.assertEqual(step_obj.evaluate(), 42)

class TestRamp(unittest.TestCase):
    def test_evaluate(self):
        ramp_obj = ramp()
        self.assertEqual(ramp_obj.evaluate(5), 5)
        self.assertEqual(ramp_obj.evaluate(10), 10)  # Ramp function should return 0 for negative input

class TestSinusoidal(unittest.TestCase):
    def test_evaluate(self):
        sinusoidal_obj = sinusoidal()
        self.assertAlmostEqual(sinusoidal_obj.evaluate(0), 0)  
        self.assertAlmostEqual(sinusoidal_obj.evaluate(math.pi), 0) 
        self.assertAlmostEqual(sinusoidal_obj.evaluate(math.pi / 2), 1) 
        self.assertAlmostEqual(sinusoidal_obj.evaluate(math.pi / 4), math.sqrt(2) / 2) 